<?php
namespace Astartsky\Paginator;

abstract class Paginator implements \Iterator, \Countable
{
    protected $total;
    protected $perPage;
    protected $current;

    protected $pages = array();
    protected $i;
    protected $limitPagesToView;

    /**
     * @param int $total
     * @param int $perPage
     * @param int $current
     * @param int $limit
     * @throws Exception
     */
    public function __construct($total, $perPage, $current, $limit = 10)
    {
        if ($perPage <= 0) {
            throw new Exception("`perPage` is less or equal to zero");
        }

        $this->limitPagesToView = (int) $limit;
        $this->total = (int) $total;
        $this->perPage = (int) $perPage;
        $this->current = (int) $current;
    }

    /**
     * @return Page[]
     */
    protected function getPages()
    {
        $pages = array();
        $pagesNum = (int) ceil($this->total / $this->perPage);

        /** show all pages from first to last */
        if ($pagesNum < $this->limitPagesToView) {
            $startI = 0;
            $stopI = $pagesNum;
        /** show only a set of pages */
        } else {

            /** trying to find left border */
            $startI = (int) ($this->current - (ceil($this->limitPagesToView/2)));
            if ($startI < 0) {
                $startI = 0;
            }

            /** trying to find right border */
            $stopI = (int) ($startI + $this->limitPagesToView);
            if ($stopI > $pagesNum) {
                $stopI = $pagesNum;
            }

            /** border conditions for right border */
            if ($stopI >= $this->total) {
                $stopI = $this->total;
                $startI = $stopI - $this->limitPagesToView;
                if ($startI < 0) {
                    $startI = 0;
                }
            }
        }

        for ($i = $startI; $i < $stopI; $i++) {
            $pages[$i] = $this->createPage($i);
        }

        return $pages;
    }

    /**
     * @param int $i
     * @return Page[]
     */
    abstract protected function createPage($i);

    /**
     * @return Page|null
     */
    public function getNext()
    {
        if (empty($this->pages)) {
            $this->rewind();
        }

        return isset($this->pages[$this->current]) ? $this->pages[$this->current] : null;
    }

    /**
     * @return Page|null
     */
    public function getPrev()
    {
        if (empty($this->pages)) {
            $this->rewind();
        }

        return isset($this->pages[$this->current - 2]) ? $this->pages[$this->current - 2] : null;
    }

    /**
     * @return int
     */
    public function getCurrentIndex()
    {
        return $this->current;
    }

    /**
     * @return Page
     */
    public function current()
    {
        return $this->pages[$this->i];
    }

    public function next()
    {
        $this->i++;
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->i;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->pages[$this->i]);
    }

    public function rewind()
    {
        $this->pages = $this->getPages();
        $keys = array_keys($this->pages);
        $this->i = count($keys) > 0 ? min($keys) : null;
    }

    /**
     * @return int
     */
    public function count()
    {
        if (empty($this->pages)) {
            $this->rewind();
        }

        return count($this->pages);
    }
}
