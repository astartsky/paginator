<?php
namespace Astartsky\Paginator;

class Page
{
    protected $index;
    protected $isActive;
    protected $url;

    /**
     * @param int $index
     * @param bool $isActive
     * @param string $url
     */
    public function __construct($index, $isActive, $url)
    {
        $this->url = $url;
        $this->index = $index;
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
